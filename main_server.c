/*
** server.c for libsock in /home/chartier/Programming/libsock
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Sun Apr  8 20:20:50 2018 CHARTIER Rodolphe
** Last update Tue Apr 10 14:25:08 2018 CHARTIER Rodolphe
*/

#include <stdlib.h>
#include <stdio.h>

#include "libsock.h"

static int	usage(int ret)
{
	printf("usage: server port\n");
	return (ret);
}

int			main(int ac, char **av)
{
	int		sfd;
	int		cfd;
	int		stop;
	char	buff[1024];

	if (ac != 2)
		return (usage(1));
	if ((sfd = server_init(atoi(av[1]), 16)) == -1)
		return (1);
	if ((cfd = accept(sfd, NULL, NULL)) == -1)
		return (1);
	memset(buff, 0, sizeof(buff));
	stop = 0;
	while (!stop && tcp_recv_str(cfd, buff, sizeof(buff)) > 0)
	{
		printf("Client: %s\n", buff);
		if (!strcmp(buff, "exit"))
			stop = 1;
	}
	close(cfd);
	close(sfd);
	return (0);
}
