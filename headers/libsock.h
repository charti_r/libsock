/*
** libsock.h for libsock in /home/chartier/Programming/libsock
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Sat Apr  7 12:09:02 2018 CHARTIER Rodolphe
** Last update Fri Apr 13 09:29:59 2018 CHARTIER Rodolphe
*/

#ifndef _LIBSOCK_H_
# define _LIBSOCK_H_

# include <string.h>
# include <stdlib.h>
# include <unistd.h>

# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <netdb.h>

typedef struct sockaddr_in	t_sockaddr_in;
typedef struct sockaddr		t_sockaddr;
typedef struct in_addr		t_in_addr;
typedef struct hostent		t_hostent;
typedef struct timeval		t_timeval;

/* typedef void				(*t_fptr)(int sfd, int fd, fd_set *fds, void *data); */
/* typedef void				(*t_ftime)(int sfd, t_clients *cs, t_timeval *to); */

/* typedef enum				e_fds_type */
/* { */
/* 	NONE					= (0), */
/* 	RDSET					= (1 << 0), */
/* 	WRSET					= (1 << 1), */
/* 	EXSET					= (1 << 2), */
/* 	ALLSET					= RDSET | WRSET | EXSET */
/* }							t_fds_type; */

/* typedef struct				s_clients */
/* { */
/* 	fd_set					*fds[3]; */
/* 	t_fptr					*fptr; */
/* 	void					**data; */
/* 	size_t					nb; */
/* 	size_t					sfd_max; */
/* }							t_clients; */

int		client_init(char *hostname, unsigned short port);

int		server_init(unsigned short port, int backlog);
/* int		select_clients(int sfd, t_clients *cs, t_timeval *to, t_ftime ftime) */
/* int		accept_clients(int sfd, t_clients *cs, t_sockaddr *csin, size_t *size); */
/* int		init_clients(int sfd, t_clients *cs, size_t nb, t_fds_type fds_type); */
/* void	free_clients(int sfd, t_clients *cs); */

int		tcp_send_str(int sfd, char *str);
int		tcp_send_data(int sfd, void *data, size_t size);
int		tcp_recv_str(int sfd, char *str, size_t size);
int		tcp_recv_data(int sfd, void *data, size_t size);

#endif // !_LIBSOCK_H_
