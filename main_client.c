/*
** main.c for libsock in /home/chartier/Programming/libsock
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Sat Apr  7 12:15:35 2018 CHARTIER Rodolphe
** Last update Tue Apr 10 15:01:16 2018 CHARTIER Rodolphe
*/

#include <stdlib.h>
#include <stdio.h>

#include "libsock.h"

static int	usage(int ret)
{
	printf("usage: client ip port\n");
	return (ret);
}

static int	my_read(int fd, char *buff, unsigned int size)
{
	int		ret;

	if ((ret = read(fd, buff, size - 1)) == -1)
		return (-1);
	buff[ret ? ret - 1 : 0] = '\0';
	return (ret);
}

int			main(int ac, char **av)
{
	int		sfd;
	int		stop;
	char	buff[1024];

	if (ac != 3)
		return (usage(1));
	if ((sfd = client_init(av[1], atoi(av[2]))) == -1)
		return (1);
	memset(buff, 0, sizeof(buff));
	stop = 0;
	while (!stop && my_read(0, buff, sizeof(buff)) > 0)
	{
		tcp_send_str(sfd, buff);
		if (!strcmp(buff, "exit"))
			stop = 1;
	}
	close(sfd);
	return (0);
}
