/*
** tcpio.c for libsock in /home/chartier/Programming/libsock
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Tue Apr 10 11:01:17 2018 CHARTIER Rodolphe
** Last update Fri Apr 13 09:30:43 2018 CHARTIER Rodolphe
*/

#include "libsock.h"

int		tcp_send_str(int sfd, char *str)
{
	return (send(sfd, str, strlen(str), 0));
}

int		tcp_send_data(int sfd, void *data, size_t size)
{
	return (send(sfd, data, size, 0));
}

int		tcp_recv_str(int sfd, char *str, size_t size)
{
	int	ret;

	if ((ret = recv(sfd, str, size - 1, 0)) == -1)
		return (-1);
	str[ret] = '\0';
	return (ret);
}

int		tcp_recv_data(int sfd, void *data, size_t size)
{
	return (recv(sfd, data, size, 0));
}
