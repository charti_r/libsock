/*
** client.c for libsock in /home/chartier/Programming/libsock
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Tue Apr 10 14:23:47 2018 CHARTIER Rodolphe
** Last update Tue Apr 10 14:23:47 2018 CHARTIER Rodolphe
*/

#include "libsock.h"

int					client_init(char *hostname, unsigned short port)
{
	int				sfd;
	t_sockaddr_in	sin;
	t_hostent		*hostinfo;

	if ((hostinfo = gethostbyname(hostname)) == NULL)
		return (-1);
	memset(&sin, 0, sizeof(sin));
	sin.sin_addr = *((t_in_addr *)hostinfo->h_addr);
	sin.sin_port = htons(port);
	sin.sin_family = AF_INET;
	if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		return (-1);
	if (connect(sfd, ((t_sockaddr *)&sin), sizeof(t_sockaddr)) == -1)
		return (-1);
	return (sfd);
}
