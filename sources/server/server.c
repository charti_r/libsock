/*
** server.c for libsock in /home/chartier/Programming/libsock
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Tue Apr 10 14:23:41 2018 CHARTIER Rodolphe
** Last update Fri Apr 13 09:29:38 2018 CHARTIER Rodolphe
*/

#include "libsock.h"

int					server_init(unsigned short port, int backlog)
{
	int				sfd;
	t_sockaddr_in	sin;

	memset(&sin, 0, sizeof(sin));
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_port = htons(port);
	sin.sin_family = AF_INET;
	if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		return (-1);
	if (bind(sfd, ((t_sockaddr *)&sin), sizeof(sin)) == -1)
		return (-1);
	if (listen(sfd, backlog) == -1)
		return (-1);
	return (sfd);
}

/* int		select_clients(int sfd, t_clients *cs, t_timeval *to, t_ftime ftime) */
/* { */
/* 	int	ret; */
/* 	int	i; */
/* 	int	isfd; */

/* 	if ((ret = select(cd->sfd_max, cs->fds[0], cs->fds[1], cs->fds[2], to)) == -1) */
/* 		return (-1); */
/* 	if (ftime && !ret) */
/* 		ftime(sfd, -1, NULL, NULL); */
/* 	isfd = -1; */
/* 	while (++isfd < cs->sfd_max) */
/* 	{ */
/* 		i = -1; */
/* 		while (++i < 3) */
/* 			if (FD_ISSET(isfd, cs->fds[i]) && cs->fptr[isfd]) */
/* 				cs->fptr[isfd](sfd, isfd, cs->fds[i], cs->data[isfd]); */
/* 	} */
/* 	return (ret); */
/* } */

/* int		accept_clients(int sfd, t_clients *cs, void *data, t_fptr fptr) */
/* { */
/* 	int	csfd; */
/* 	int	i; */

/* 	if ((csfd = accept(sfd, csin, size)) == -1) */
/* 		return (-1); */
/* 	i = -1; */
/* 	while (++i < 3) */
/* 		if (cs->fds[i]) */
/* 			FD_SET(csfd, cs->fds[i]); */
/* 	if (csfd >= cs->nb) */
/* 	{ */
/* 		cs->nb = csfd + 1; */
/* 		if ((cs->data = realloc(cs->data, sizeof(*cs->data) * cs->nb)) == NULL) */
/* 			return (-1); */
/* 		if ((cs->fptr = realloc(cs->fptr, sizeof(*cs->fptr) * cs->nb)) == NULL) */
/* 			return (-1); */
/* 	} */
/* 	cs->data[csfd] = data; */
/* 	cs->fptr[csfd] = fptr; */
/* 	cs->sfd_max = csfd + 1 > cs->sfd_max ? csfd + 1 : cs->sfd_max; */
/* 	return (0); */
/* } */

/* int		init_clients(int sfd, t_clients *cs, size_t nb, t_fds_type fds_type) */
/* { */
/* 	int	i; */

/* 	if (!cs) */
/* 		return (-1); */
/* 	i = -1; */
/* 	while (++i < 3) */
/* 	{ */
/* 		if (!(fds_type & (1 << i))) */
/* 			cs->fds[i] = NULL; */
/* 		else if ((cs->fds[i] = malloc(sizeof(**cs->fds))) == NULL) */
/* 			return (-1); */
/* 		if (cs->fds[i]) */
/* 		{ */
/* 			FD_ZERO(cs->fds[i]); */
/* 			FD_SET(sfd, cs->fds[i]); */
/* 		} */
/* 	} */
/* 	if ((cs->data = realloc(NULL, sizeof(*cs->data) * nb)) == NULL) */
/* 		return (-1); */
/* 	if ((cs->fptr = realloc(NULL, sizeof(*cs->fptr) * nb)) == NULL) */
/* 		return (-1); */
/* 	cs->nb = nb; */
/* 	cs->sfd_max = sfd + 1; */
/* 	return (0); */
/* } */

/* void	free_clients(int sfd, t_clients *cs) */
/* { */
/* 	int	i; */

/* 	if (!cs) */
/* 		return ; */
/* 	i = -1; */
/* 	while (++i < 3) */
/* 		if (cs->fds[i]) */
/* 		{ */
/* 			FD_ZERO(cs->fds[i]); */
/* 			free(cs->fds[i]); */
/* 			cs->fds[i] = NULL; */
/* 		} */
/* 	if (cs->data) */
/* 		free(cs->data); */
/* 	if (cs->fptr) */
/* 		free(cs->fptr); */
/* } */
