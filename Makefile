##
## Makefile for libsock in /home/chartier/Programming/libsock
##
## Made by CHARTIER Rodolphe
## Login   <charti_r@etna-alternance.net>
##
## Started on  Tue Apr 10 11:11:29 2018 CHARTIER Rodolphe
## Last update Fri Apr 13 09:31:41 2018 CHARTIER Rodolphe
##

INC			= -I ./headers
CFLAGS		= -Wall -Wextra -Werror $(INC)

CC			= gcc
RM			= rm -f

NAME		= libsock.a
NAME_CLT	= client.out
NAME_SRV	= server.out

SRC_DIR		= sources
SRC_CLT_DIR	= $(SRC_DIR)/client
SRC_SRV_DIR	= $(SRC_DIR)/server

SRC			= \
			$(SRC_DIR)/tcpio.c \
			$(SRC_CLT_DIR)/client.c \
			$(SRC_SRV_DIR)/server.c

OBJ			= $(SRC:.c=.o)

all: $(NAME) $(NAME_CLT) $(NAME_SRV)

$(NAME): $(OBJ)
	ar rcs $(NAME) $(OBJ)

$(NAME_CLT): $(NAME)
	$(CC) $(CFLAGS) main_client.c -L./ -lsock -o $(NAME_CLT)

$(NAME_SRV): $(NAME)
	$(CC) $(CFLAGS) main_server.c -L./ -lsock -o $(NAME_SRV)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)
	$(RM) $(NAME_CLT)
	$(RM) $(NAME_SRV)

re: fclean all

.PHONY: all $(NAME) $(NAME_CLT) $(NAME_SRV) clean fclean re
